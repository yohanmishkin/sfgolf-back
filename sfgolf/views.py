"""
Routes and views for the flask application.
"""

from datetime import datetime
from flask import render_template

# /token
# /users
# /golfers
# /teams

# email (Flask-Mail
# celery ?
# user management ?
# password security
# environment configuration
# form validation ?
# blueprints
# tests
# db migrations

@app.route('/')
@app.route('/home')
def home():
    """Renders the home page."""
    return render_template(
        'index.html',
        title='Home Page',
        year=datetime.now().year,
    )

